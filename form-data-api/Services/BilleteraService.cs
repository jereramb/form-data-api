﻿using FormDataAPI.Models;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace FormDataAPI.Services
{
    public class BilleteraService : IBilleteraService
    {
        protected readonly ILogger<BilleteraService> _logger;
        private readonly string _url;

        public BilleteraService(ILogger<BilleteraService> logger)
        {
            _logger = logger;
            _url = "https://localhost:5041";
        }

        private MultipartFormDataContent GetContent(AltaPersonaRequest altaPersonaRequest, Foto fotos)
        {
            var form = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(altaPersonaRequest));

            var fotoRostro = new ByteArrayContent(fotos.fotoRostro)
            {
                Headers = { ContentType = MediaTypeHeaderValue.Parse("image/jpeg") }
            };

            var fotoDniTrasera = new ByteArrayContent(fotos.fotoDniFrontal)
            {
                Headers = { ContentType = MediaTypeHeaderValue.Parse("image/jpeg") }
            };

            var fotoDniFrontal = new ByteArrayContent(fotos.fotoDniFrontal)
            {
                Headers = { ContentType = MediaTypeHeaderValue.Parse("image/jpeg") }
            };

            return new MultipartFormDataContent("----dio-boundary-" + DateTime.Now.ToString(CultureInfo.InvariantCulture))
            {
                { new ByteArrayContent(form), "altaPersonaRequest" },
                { fotoDniFrontal, "fotoDniFrontal", "dni0.jpg" },
                { fotoDniTrasera, "fotoDniTrasera", "dni1.jpg" },
                { fotoRostro, "fotoRostro", "rostro.jpg" },
            };
        }

        private HttpClient GetHttpClient()
        {
            var handler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; }
            };

            var httpClient = new HttpClient(handler);

            return httpClient;
        }

        public async Task<string> SendRequest(AltaPersonaRequest altaPersonaRequest, Foto fotos)
        {
            var url = $"{_url}/SolicitudesAltaUsuario/ProcesoAlta";

            var request = new HttpRequestMessage(HttpMethod.Post, new Uri(url))
            {
                Content = GetContent(altaPersonaRequest, fotos)
            };

            HttpResponseMessage response;
            try
            {
                using (var httpClient = GetHttpClient())
                {
                    response = await httpClient.SendAsync(request);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                throw new Exception(e.Message, e);
            }

            if (response.StatusCode != HttpStatusCode.OK)
            {
                _logger.LogWarning($"{response.ReasonPhrase}");
                throw new Exception($"{response.ReasonPhrase}");
            }

            return await response.Content.ReadAsStringAsync();
        }
    }
}
