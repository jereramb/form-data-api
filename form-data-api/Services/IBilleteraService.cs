﻿using FormDataAPI.Models;
using System.Threading.Tasks;

namespace FormDataAPI.Services
{
    public interface IBilleteraService
    {
        Task<string> SendRequest(AltaPersonaRequest altaPersonaRequest, Foto fotos);
    }
}
