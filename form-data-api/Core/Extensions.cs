﻿using FormDataAPI.FeatureProviders;
using FormDataAPI.Serializer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Globalization;

namespace FormDataAPI.Core
{
    public static class Extensions
    {
        public static void AddJsonConfiguration(this IMvcBuilder mvcBuilder)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings()
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            mvcBuilder.AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
            });

            mvcBuilder.AddMvcOptions(o => o.ModelBinderProviders.Insert(0, new DateTimeModelBinderProvider()));
        }

        public static void AddCustomControllerFeatureProvider(this IMvcBuilder mvcBuilder)
        {
            mvcBuilder.ConfigureApplicationPartManager(manager =>
            {
                manager.FeatureProviders.Clear();
                manager.FeatureProviders.Add(new CustomControllerFeatureProvider());
            });
        }

        public static void AddCulture(this IServiceCollection services)
        {
            var culture = "es-AR";

            services.Configure<RequestLocalizationOptions>(
                   options =>
                   {
                       options.DefaultRequestCulture = new RequestCulture(culture: culture, uiCulture: culture);
                       options.SupportedCultures.Add(new CultureInfo(culture));
                       options.SupportedUICultures.Add(new CultureInfo(culture));
                   });
        }
    }
}
