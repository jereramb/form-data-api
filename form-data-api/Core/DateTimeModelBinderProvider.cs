﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Globalization;

namespace FormDataAPI.Serializer
{
    public class DateTimeModelBinderProvider : IModelBinderProvider
    {
        // You could make this a property to allow customization
        private static readonly DateTimeStyles SupportedStyles = DateTimeStyles.AdjustToUniversal | DateTimeStyles.AllowWhiteSpaces;

        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            var modelType = context.Metadata.UnderlyingOrModelType;
            if (modelType == typeof(DateTime))
                return new UtcAwareDateTimeModelBinder(SupportedStyles);

            return null;
        }
    }
}