﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace FormDataAPI.Serializer
{
    public class DateTimeConverter : JsonConverter<DateTime>
    {
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            reader.TryGetDateTime(out DateTime value);

            if (value.Kind != DateTimeKind.Utc)
                value = DateTime.SpecifyKind(value.ToUniversalTime(), DateTimeKind.Utc);

            return value;
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value);
        }
    }
}