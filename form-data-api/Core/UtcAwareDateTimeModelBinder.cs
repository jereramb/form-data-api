﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace FormDataAPI.Serializer
{
    internal class UtcAwareDateTimeModelBinder : IModelBinder
    {
        private readonly DateTimeStyles _supportedStyles;

        public UtcAwareDateTimeModelBinder(DateTimeStyles supportedStyles)
        {
            _supportedStyles = supportedStyles;
        }

        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
                throw new ArgumentNullException(nameof(bindingContext));

            var modelName = bindingContext.ModelName;
            var valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
            if (valueProviderResult == ValueProviderResult.None)
                return Task.CompletedTask;

            var modelState = bindingContext.ModelState;
            modelState.SetModelValue(modelName, valueProviderResult);

            var metadata = bindingContext.ModelMetadata;
            var type = metadata.UnderlyingOrModelType;

            var value = valueProviderResult.FirstValue;
            var culture = valueProviderResult.Culture;

            if (type != typeof(DateTime))
                throw new NotSupportedException();

            if (string.IsNullOrWhiteSpace(value))
                if (metadata.IsReferenceOrNullableType)
                    return Task.CompletedTask;

            var couldParse = TryParse(value, culture);

            if (!couldParse)
            {
                modelState.TryAddModelError(
                    modelName,
                    metadata.ModelBindingMessageProvider.ValueIsInvalidAccessor(valueProviderResult.ToString()));

                return Task.CompletedTask;
            }

            var model = Convert(value, culture);

            // When converting value, a null model may indicate a failed conversion for an otherwise required
            // model (can't set a ValueType to null). This detects if a null model value is acceptable given the
            // current bindingContext. If not, an error is logged.
            if (model == null && !metadata.IsReferenceOrNullableType)
                modelState.TryAddModelError(
                    modelName,
                    metadata.ModelBindingMessageProvider.ValueMustNotBeNullAccessor(valueProviderResult.ToString()));
            else
                bindingContext.Result = ModelBindingResult.Success(model);

            return Task.CompletedTask;
        }

        private bool TryParse(string value, CultureInfo culture)
        {
            try
            {
                var fecha = DateTime.Parse(value, culture, _supportedStyles);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private object Convert(string value, CultureInfo culture)
        {
            if (string.IsNullOrWhiteSpace(value))
                return null;

            DateTime fecha;

            try
            {
                fecha = DateTime.Parse(value, culture, _supportedStyles);
            }
            catch (Exception)
            {
                return null;
            }

            if (fecha.Kind != DateTimeKind.Utc)
                fecha = DateTime.SpecifyKind(fecha.ToUniversalTime(), DateTimeKind.Utc);

            return fecha;
        }
    }
}