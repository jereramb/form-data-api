﻿using Microsoft.AspNetCore.Mvc.Controllers;
using System;
using System.Linq;
using System.Reflection;

namespace FormDataAPI.FeatureProviders
{
    public class CustomControllerFeatureProvider : ControllerFeatureProvider
    {
        private static Type _accountControllerBase;
        private static Type AccountControllerBase
        {
            get
            {
                if (_accountControllerBase == null)
                {
                    var coreAssembly = Assembly.GetExecutingAssembly();
                    var coreSolutionAccountController = coreAssembly.GetTypes().FirstOrDefault(t => t.GetInterfaces().Contains(typeof(IAccountController)));
                    _accountControllerBase = coreSolutionAccountController;
                }

                return _accountControllerBase;
            }
        }

        private static Type _accountControllerChild;
        private static Type AccountControllerChild
        {
            get
            {
                if (_accountControllerChild == null)
                {
                    var runningAssembly = Assembly.GetEntryAssembly();
                    var childSolutionAccountController = runningAssembly.GetTypes().FirstOrDefault(t => t.GetInterfaces().Contains(typeof(IAccountController)));
                    _accountControllerChild = childSolutionAccountController;
                }

                return _accountControllerChild;
            }
        }


        protected override bool IsController(TypeInfo typeInfo)
        {
            var isController = base.IsController(typeInfo);

            if (!isController)
                return isController;

            if (AccountControllerChild != null &&
                AccountControllerBase != null &&
                typeInfo.FullName == AccountControllerBase.FullName)
                return false;

            return isController;
        }
    }
}