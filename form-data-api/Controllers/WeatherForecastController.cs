﻿using FormDataAPI.Models;
using FormDataAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormDataAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly IBilleteraService _billeteraService;

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(
            ILogger<WeatherForecastController> logger,
            IBilleteraService billeteraService)
        {
            _logger = logger;
            _billeteraService = billeteraService;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }

        [HttpGet("Test")]
        public async Task<IActionResult> Test()
        {
            var form = new AltaPersonaRequest()
            {
                Apellido = "Ramb",
                CaracteristicaPaisTelefono = "54",
                CodigoAreaTelefono = "336",
                Cuit = 20388487268,
                Email = "jere@gmail.com",
                EntidadTipoDocumento = Guid.Parse("DFC416D2-3B77-43EA-9C2B-7A593205A3AC"),
                FechaNacimiento = DateTime.Parse("2022-01-01"),
                IdEntidad = Guid.Parse("FF7DA4B0-6799-4243-ADF7-C230F00CDC4F"),
                IdTipoPersona = Guid.Parse("8BBBF35E-DDB7-4E96-B31F-CE22E96DC2EC"),
                Nombre = "Jeremias",
                NumeroDocumento = "38848726",
                NumeroTelefono = "4397904",
                Password = "Test2022",
                RazonSocial = "Jeremias Ramb",
                Sexo = "M"
            };

            var foto = await GetFoto();

            await _billeteraService.SendRequest(form, foto);

            return Ok();
        }

        private async Task<Foto> GetFoto()
        {
            try
            {
                return new Foto()
                {
                    fotoDniFrontal = await System.IO.File.ReadAllBytesAsync("./Assets/Images/dni0.jpg"),
                    fotoDniTrasera = await System.IO.File.ReadAllBytesAsync("./Assets/Images/dni1.jpg"),
                    fotoRostro = await System.IO.File.ReadAllBytesAsync("./Assets/Images/rostro.jpg"),
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
