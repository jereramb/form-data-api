﻿using System;

namespace FormDataAPI.Models
{
    public class AltaPersonaRequest
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string RazonSocial { get; set; }
        public string Sexo { get; set; }
        public Guid EntidadTipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public long Cuit { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string CaracteristicaPaisTelefono { get; set; }
        public string CodigoAreaTelefono { get; set; }
        public string NumeroTelefono { get; set; }
        public Guid IdEntidad { get; set; }
        public Guid IdTipoPersona { get; set; }
    }
}
