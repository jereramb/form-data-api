﻿namespace FormDataAPI.Models
{
    public class Foto
    {
        public byte[] fotoRostro { get; set; }
        public byte[] fotoDniTrasera { get; set; }
        public byte[] fotoDniFrontal { get; set; }
    }
}
